package com.api.medicationmanagement.controllers;

import com.api.medicationmanagement.dtos.ManufacturerDto;
import com.api.medicationmanagement.services.ManufacturerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "Endpoint of Manufacturer", description = "Endpoint used to perform operations on the Manufacturers entity")
@RequestMapping(value = "/manufacturers")
public class ManufacturerController {

    @Autowired
    private ManufacturerService service;

    @GetMapping
    @Operation(summary = "Search all Manufacturers", description = "This method is used to search all Manufacturers")
    public ResponseEntity<List<ManufacturerDto>> findAll() {
        List<ManufacturerDto> list = service.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Search a Manufacturer by id", description = "This method is used to search a Manufacturer by id")
    public ResponseEntity<ManufacturerDto> findById(@PathVariable Long id) {
        ManufacturerDto dto = service.findById(id);
        return ResponseEntity.ok().body(dto);
    }

    @PostMapping
    @Operation(summary = "Insert a new Manufacturer", description = "This method is used to insert a new Manufacturer")
    public ResponseEntity<ManufacturerDto> insert(@RequestBody ManufacturerDto dto) {
        dto = service.insert(dto);
        return ResponseEntity.ok().body(dto);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "Update a Manufacturer", description = "This method is used to update a Manufacturer")
    public ResponseEntity<ManufacturerDto> update(@PathVariable Long id, @RequestBody ManufacturerDto dto) {
        dto = service.update(id, dto);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "Delete a Manufacturer", description = "This method is used to delete a Manufacturer")
    public ResponseEntity<ManufacturerDto> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
