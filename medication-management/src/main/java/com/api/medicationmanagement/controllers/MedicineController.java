package com.api.medicationmanagement.controllers;

import com.api.medicationmanagement.dtos.MedicineDto;
import com.api.medicationmanagement.services.MedicineService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@Tag(name = "Endpoint of Medicine", description = "Endpoint used to perform operations in the Medicines entity")
@RequestMapping(value = "/medicines")
public class MedicineController {

    @Autowired
    private MedicineService service;

    @GetMapping
    @Operation(summary= "Search all Medicines", description = "This method is used to search all Medicines")
    public ResponseEntity<Page<MedicineDto>> findAll(Pageable pageable) {
        Page<MedicineDto> list = service.findAllPaged(pageable);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/search")
    @Operation(summary = "Search Medicines by name", description = "This method is used to search Medicines by name")
    public ResponseEntity<Page<MedicineDto>> findByName(
            @RequestParam(value = "name", defaultValue = "") String name,
            Pageable pageable
    ) {
        Page<MedicineDto> list = service.searchByName(name.trim(), pageable);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    @Operation(summary= "Search Medicine by id", description = "This method is used to search Medicine by id")
    public ResponseEntity<MedicineDto> findById(@PathVariable Long id) {
        MedicineDto dto = service.findById(id);
        return ResponseEntity.ok().body(dto);
    }

    @PostMapping
    @Operation(summary = "Insert a new Medicine", description = "This method is used to insert a new Medicine")
    public ResponseEntity<MedicineDto> insert(@RequestBody @Valid MedicineDto dto) {
        dto = service.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dto.getId()).toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "Update a Medicine", description = "This method is used to update a Medicine")
    public ResponseEntity<MedicineDto> update(@PathVariable Long id, @RequestBody MedicineDto dto) {
        dto = service.update(id, dto);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "Delete a Medicine",description= "This method is used to delete a Medicine")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

}
