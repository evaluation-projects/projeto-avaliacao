package com.api.medicationmanagement.controllers;

import com.api.medicationmanagement.dtos.AdverseReactionsDto;
import com.api.medicationmanagement.services.AdverseReactionsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@Tag(name = "Endpoint of Adverse Reactions", description = "Endpoint used to perform operations in the Adverse Reactions entity")
@RequestMapping(value = "/adverse-reactions")
public class AdverseReactionsController {

    @Autowired
    private AdverseReactionsService service;

    @GetMapping
    @Operation(summary = "Find all adverse reactions", description = "This method is used to find all adverse reactions")
    public ResponseEntity<Page<AdverseReactionsDto>> findAll(Pageable pageable) {
        Page<AdverseReactionsDto> list = service.findAllPaged(pageable);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Find adverse reactions by id", description = "This method is used to find adverse reactions by id")
    public ResponseEntity<AdverseReactionsDto> findById(@PathVariable Long id) {
        AdverseReactionsDto dto = service.findById(id);
        return ResponseEntity.ok().body(dto);
    }

    @PostMapping
    @Operation(summary  = "Insert adverse reactions", description = "This method is used to insert adverse reactions")
    public ResponseEntity<AdverseReactionsDto> insert(@RequestBody AdverseReactionsDto dto) {
        dto = service.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dto.getId()).toUri();
        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary  = "Update adverse reactions", description = "This method is used to update adverse reactions")
    public ResponseEntity<AdverseReactionsDto> update(@PathVariable Long id, @RequestBody AdverseReactionsDto dto) {
        dto = service.update(id, dto);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary  = "Delete adverse reactions", description= "This method is used to delete adverse reactions")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
