package com.api.medicationmanagement.models.repositories;

import com.api.medicationmanagement.models.entities.Medicine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicineRepository extends JpaRepository<Medicine, Long> {


    @Query(value = "select m from Medicine m where lower(m.name) like lower(concat('%',:name,'%'))")
    Page<Medicine> searchByName(String name, Pageable pageable);
}
