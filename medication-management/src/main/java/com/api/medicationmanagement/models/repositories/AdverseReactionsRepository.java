package com.api.medicationmanagement.models.repositories;

import com.api.medicationmanagement.models.entities.AdverseReactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdverseReactionsRepository extends JpaRepository<AdverseReactions, Long> {
}
