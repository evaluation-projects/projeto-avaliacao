package com.api.medicationmanagement.services;

import com.api.medicationmanagement.dtos.AdverseReactionsDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AdverseReactionsService {
    Page<AdverseReactionsDto> findAllPaged(Pageable pageable);

    AdverseReactionsDto findById(Long id);

    AdverseReactionsDto insert(AdverseReactionsDto dto);

    AdverseReactionsDto update(Long id, AdverseReactionsDto dto);

    void delete(Long id);
}
