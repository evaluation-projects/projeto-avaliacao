package com.api.medicationmanagement.services.impl;

import com.api.medicationmanagement.dtos.AdverseReactionsDto;
import com.api.medicationmanagement.models.entities.AdverseReactions;
import com.api.medicationmanagement.models.repositories.AdverseReactionsRepository;
import com.api.medicationmanagement.services.AdverseReactionsService;
import com.api.medicationmanagement.services.exceptions.DatabaseException;
import com.api.medicationmanagement.services.exceptions.ResourseNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AdverseReactionsServiceImpl implements AdverseReactionsService {

    @Autowired
    private AdverseReactionsRepository repository;

    @Transactional(readOnly = true)
    public Page<AdverseReactionsDto> findAllPaged(Pageable pageable) {
        Page<AdverseReactions> list = repository.findAll(pageable);
        return list.map(AdverseReactionsDto::new);
    }

    @Transactional(readOnly = true)
    public AdverseReactionsDto findById(Long id) {
        try {
            Optional<AdverseReactions> obj = Optional.of(repository.getById(id));
            AdverseReactions entity = obj.orElseThrow(() -> new ResourseNotFoundException("Id not found " + id));
            return new AdverseReactionsDto(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        }
    }

    @Transactional
    public AdverseReactionsDto insert(AdverseReactionsDto dto) {
        AdverseReactions entity = new AdverseReactions();
        entity.setDescription(dto.getDescription());
        entity = repository.save(entity);
        return new AdverseReactionsDto(entity);
    }

    @Transactional
    public AdverseReactionsDto update(Long id, AdverseReactionsDto dto) {
        try {
            AdverseReactions entity = repository.getOne(id);
            entity.setDescription(dto.getDescription());
            entity = repository.save(entity);
            return new AdverseReactionsDto(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        }
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }
}
