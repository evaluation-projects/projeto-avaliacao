package com.api.medicationmanagement.services;

import com.api.medicationmanagement.dtos.MedicineDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MedicineService {

    Page<MedicineDto> findAllPaged(Pageable pageable);

    Page<MedicineDto> searchByName(String name, Pageable pageable);

    MedicineDto findById(Long id);

    MedicineDto insert(MedicineDto dto);

    MedicineDto update(Long id, MedicineDto dto);

    void delete(Long id);

}
