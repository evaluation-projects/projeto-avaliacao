package com.api.medicationmanagement.services.impl;

import com.api.medicationmanagement.dtos.ManufacturerDto;
import com.api.medicationmanagement.models.entities.Manufacturer;
import com.api.medicationmanagement.models.repositories.ManufacturerRepository;
import com.api.medicationmanagement.services.ManufacturerService;
import com.api.medicationmanagement.services.exceptions.DatabaseException;
import com.api.medicationmanagement.services.exceptions.ResourseNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    @Autowired
    private ManufacturerRepository repository;


    @Transactional(readOnly = true)
    public List<ManufacturerDto> findAll() {
        List<Manufacturer> list = repository.findAll();
        return list.stream().map(ManufacturerDto::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public ManufacturerDto findById(Long id) {
        try {
           Optional<Manufacturer> obj = Optional.of(repository.getById(id));
            Manufacturer entity = obj.orElseThrow(() -> new ResourseNotFoundException("Id not found " + id));
            return new ManufacturerDto(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        }
    }

    @Transactional
    public ManufacturerDto insert(ManufacturerDto dto) {
        Manufacturer entity = new Manufacturer();
        entity.setName(dto.getName());
        entity = repository.save(entity);
        return new ManufacturerDto(entity);
    }

    @Transactional
    public ManufacturerDto update(Long id, ManufacturerDto dto) {
        try {
            Manufacturer entity = repository.getOne(id);
            entity.setName(dto.getName());
            entity = repository.save(entity);
            return new ManufacturerDto(entity);
        } catch (Exception e) {
            throw new ResourseNotFoundException("Id not found " + id);
        }
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }
}
