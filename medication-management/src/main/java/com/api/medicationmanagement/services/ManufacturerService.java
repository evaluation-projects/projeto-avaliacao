package com.api.medicationmanagement.services;

import com.api.medicationmanagement.dtos.ManufacturerDto;

import java.util.List;

public interface ManufacturerService {

    List<ManufacturerDto> findAll();

    ManufacturerDto findById(Long id);

    ManufacturerDto insert(ManufacturerDto dto);

    ManufacturerDto update(Long id, ManufacturerDto dto);

    void delete(Long id);

}
