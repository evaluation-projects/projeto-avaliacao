package com.api.medicationmanagement.services.exceptions;

import java.io.Serial;

public class ResourseNotFoundException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 1L;

    public ResourseNotFoundException(String msg) {
        super(msg);
    }
}
