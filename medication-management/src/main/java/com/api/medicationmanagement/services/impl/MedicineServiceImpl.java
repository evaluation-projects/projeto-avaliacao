package com.api.medicationmanagement.services.impl;

import com.api.medicationmanagement.dtos.AdverseReactionsDto;
import com.api.medicationmanagement.dtos.MedicineDto;
import com.api.medicationmanagement.models.entities.AdverseReactions;
import com.api.medicationmanagement.models.entities.Medicine;
import com.api.medicationmanagement.models.repositories.AdverseReactionsRepository;
import com.api.medicationmanagement.models.repositories.MedicineRepository;
import com.api.medicationmanagement.services.MedicineService;
import com.api.medicationmanagement.services.exceptions.DatabaseException;
import com.api.medicationmanagement.services.exceptions.ResourseNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
public class MedicineServiceImpl implements MedicineService {

    @Autowired
    private MedicineRepository repository;


    @Autowired
    private AdverseReactionsRepository adverseReactionsRepository;

    @Transactional(readOnly = true)
    public Page<MedicineDto> findAllPaged(Pageable pageable) {
        Page<Medicine> list = repository.findAll(pageable);
        return list.map(MedicineDto::new);
    }

    @Transactional(readOnly = true)
    public Page<MedicineDto> searchByName(String name, Pageable pageable) {
        Page<Medicine> list = repository.searchByName(name, pageable);
        return list.map(MedicineDto::new);
    }

    @Transactional(readOnly = true)
    public MedicineDto findById(Long id) {
        try {
            Optional<Medicine> obj = Optional.of(repository.getById(id));
            Medicine entity = obj.orElseThrow(() -> new ResourseNotFoundException("Entity not found"));
            return new MedicineDto(entity, entity.getAdverseReactions());
        } catch (EntityNotFoundException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        }
    }

    @Transactional
    public MedicineDto insert(MedicineDto dto) {
        Medicine entity = new Medicine();
        copyDtoToEntity(dto, entity);
        entity.setCreatedAt(Instant.now());
        entity = repository.save(entity);
        return new MedicineDto(entity);
    }

    @Transactional
    public MedicineDto update(Long id, MedicineDto dto) {
        try {
            Medicine entity = repository.getOne(id);
            copyDtoToEntity(dto, entity);
            entity.setUpdatedAt(Instant.now());
            entity = repository.save(entity);
            return new MedicineDto(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        }
    }

    private void copyDtoToEntity(MedicineDto dto, Medicine entity) {
        entity.setRegister(dto.getRegister());
        entity.setName(dto.getName());
        entity.setExpirationDate(dto.getExpirationDate());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPrice(dto.getPrice());
        entity.setQuantityPills(dto.getQuantityPills());
        entity.setManufacturer(dto.getManufacturer());

        entity.getAdverseReactions().clear();

        for (AdverseReactionsDto advReact : dto.getAdverseReactions()) {
            AdverseReactions adverseReactions = adverseReactionsRepository.getOne(advReact.getId());
            entity.getAdverseReactions().add(adverseReactions);
        }
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourseNotFoundException("Id not found " + id);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }
}
