package com.api.medicationmanagement.dtos;

import com.api.medicationmanagement.models.entities.AdverseReactions;
import com.api.medicationmanagement.models.entities.Manufacturer;
import com.api.medicationmanagement.models.entities.Medicine;
import jakarta.validation.constraints.NotBlank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MedicineDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    @NotBlank
    private String register;
    @NotBlank
    private String name;
    @NotBlank
    private String expirationDate;
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String price;
    private Integer quantityPills;
    private Manufacturer manufacturer;

    private List<AdverseReactionsDto> adverseReactions = new ArrayList<>();

    public MedicineDto() {

    }

    public MedicineDto(Long id, String register, String name, String expirationDate, String phoneNumber, String price, Integer quantityPills, Manufacturer manufacturer) {
        this.id = id;
        this.register = register;
        this.name = name;
        this.expirationDate = expirationDate;
        this.phoneNumber = phoneNumber;
        this.price = price;
        this.quantityPills = quantityPills;
        this.manufacturer = manufacturer;
    }

    public MedicineDto(Medicine entity) {
        this.id = entity.getId();
        this.register = entity.getRegister();
        this.name = entity.getName();
        this.expirationDate = entity.getExpirationDate();
        this.phoneNumber = entity.getPhoneNumber();
        this.price = entity.getPrice();
        this.quantityPills = entity.getQuantityPills();
        this.manufacturer = entity.getManufacturer();
    }

    public MedicineDto(Medicine entity, Set<AdverseReactions> adverseReactions) {
        this(entity);
        adverseReactions.forEach(adverseReaction -> this.adverseReactions.add(new AdverseReactionsDto(adverseReaction)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getQuantityPills() {
        return quantityPills;
    }

    public void setQuantityPills(Integer quantityPills) {
        this.quantityPills = quantityPills;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<AdverseReactionsDto> getAdverseReactions() {
        return adverseReactions;
    }

}
