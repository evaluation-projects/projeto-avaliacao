package com.api.medicationmanagement.dtos;

import com.api.medicationmanagement.models.entities.AdverseReactions;
import com.api.medicationmanagement.models.entities.Manufacturer;

import java.io.Serializable;

public class AdverseReactionsDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String description;

    public AdverseReactionsDto() {
    }

    public AdverseReactionsDto(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public AdverseReactionsDto(AdverseReactions entity) {
        this.id = entity.getId();
        this.description = entity.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
