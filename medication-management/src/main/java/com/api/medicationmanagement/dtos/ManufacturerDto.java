package com.api.medicationmanagement.dtos;

import com.api.medicationmanagement.models.entities.Manufacturer;

import java.io.Serializable;

public class ManufacturerDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;

    public ManufacturerDto() {
    }

    public ManufacturerDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ManufacturerDto(Manufacturer entity) {
        this.id = entity.getId();
        this.name = entity.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
