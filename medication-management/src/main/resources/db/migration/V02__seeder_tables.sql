INSERT INTO tb_manufacturer ( name ) VALUES ( 'Ache' );
INSERT INTO tb_manufacturer ( name ) VALUES ( 'Bayer' );
INSERT INTO tb_manufacturer ( name ) VALUES ( 'Merck' );
INSERT INTO tb_manufacturer ( name ) VALUES ( 'Novartis' );
INSERT INTO tb_manufacturer ( name ) VALUES ( 'Pfizer' );
INSERT INTO tb_manufacturer ( name ) VALUES ( 'Sanofi-Aventis' );


-- Path: src/main/resources/import.sql

    -- second commit

INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Dor de Cabeça' );
INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Dor de Garganta' );
INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Dor de Barriga' );
INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Dor de Ouvido' );
INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Diarreia' );
INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Náusea' );
INSERT INTO tb_adverse_reactions ( description) VALUES ( 'Vômito' );

INSERT INTO tb_medicine ( created_at,expiration_date,name,phone_number, price, quantity_pills,register, manufacturer_id) VALUES (TIMESTAMP WITH TIME ZONE '2020-07-05T13:40:20Z','12/20/2020','Dipirona', '91 9 9178 2007', '2,50',20,'0.0000.0000.000-0', 1);
INSERT INTO tb_medicine ( created_at,expiration_date,name,phone_number, price, quantity_pills,register, manufacturer_id) VALUES (TIMESTAMP WITH TIME ZONE '2020-07-05T13:40:20Z','13/20/2020','Ciprofloxacino', '91 9 9278 2007', '5,00',30,'1.0000.0000.000-0', 2);
INSERT INTO tb_medicine ( created_at,expiration_date,name,phone_number, price, quantity_pills,register, manufacturer_id) VALUES (TIMESTAMP WITH TIME ZONE '2020-07-05T13:40:20Z','14/20/2020','Amoxicilina', '91 9 9378 2007', '3,00',40,'2.0000.0000.000-0', 3);
INSERT INTO tb_medicine ( created_at,expiration_date,name,phone_number, price, quantity_pills,register, manufacturer_id) VALUES (TIMESTAMP WITH TIME ZONE '2020-07-05T13:40:20Z','15/20/2020','Paracetamol', '91 9 9478 2007', '4,00',50,'3.0000.0000.000-0', 4);
INSERT INTO tb_medicine ( created_at,expiration_date,name,phone_number, price, quantity_pills,register, manufacturer_id) VALUES (TIMESTAMP WITH TIME ZONE '2020-07-05T13:40:20Z','16/20/2020','Dipirona', '91 9 9578 2007', '2,50',20,'4.0000.0000.000-0', 5);

INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 1, 1);
INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 1, 2);
INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 2, 3);
INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 3, 4);
INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 3, 5);
INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 5, 6);
INSERT INTO tb_medicine_adverse_reactions ( medicine_id,adverse_reactions_id) VALUES ( 4, 7);