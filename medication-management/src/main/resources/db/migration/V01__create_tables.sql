
DROP TABLE IF EXISTS public.tb_medicine_adverse_reactions;

DROP TABLE IF EXISTS public.tb_medicine;

DROP TABLE IF EXISTS public.tb_manufacturer;

DROP TABLE IF EXISTS public.tb_adverse_reactions;

CREATE TABLE
    IF NOT EXISTS public.tb_manufacturer (
        id SERIAL PRIMARY KEY,
        name VARCHAR(200)
    );


CREATE TABLE
    IF NOT EXISTS public.tb_medicine (
        id SERIAL PRIMARY KEY,
        register VARCHAR(17) NOT NULL,
        name VARCHAR(150),
        expiration_date VARCHAR(10),
        phone_number VARCHAR(15),
        price VARCHAR(10),
        quantity_pills INTEGER,
        created_at TIMESTAMP DEFAULT NULL,
        updated_at TIMESTAMP DEFAULT NULL,
        manufacturer_id SERIAL,
        FOREIGN KEY (manufacturer_id) REFERENCES tb_manufacturer(id)
    );

CREATE TABLE
    IF NOT EXISTS public.tb_adverse_reactions (
        id SERIAL PRIMARY KEY,
        description VARCHAR(200)
    );



CREATE TABLE
    IF NOT EXISTS public.tb_medicine_adverse_reactions (
        medicine_id INTEGER NOT NULL,
        adverse_reactions_id INTEGER NOT NULL,
        CONSTRAINT fk_tb_medicine FOREIGN KEY (medicine_id) REFERENCES tb_medicine(id),
        CONSTRAINT fk_tb_adverse_reactions FOREIGN KEY (adverse_reactions_id) REFERENCES tb_adverse_reactions(id)
    );

