package com.api.medicationmanagement.models.repositories;

import com.api.medicationmanagement.models.entities.Manufacturer;
import com.api.medicationmanagement.models.entities.Medicine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Optional;

@DataJpaTest
public class MedicineRepositoryTest {

    @Autowired
    private MedicineRepository repository;

    private long existingId;
    private long nonExistingId;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 1000L;
    }

    @Test
    @DisplayName("Should return a medicinedto when create a new medicinedto")
    public void saveShouldPersistWithAutoincrementWhenIdIsNull() {

        Medicine medicine = new Medicine(null, "0.0045.9900.000-0", "Tandrilax", "12/20/2023", "91 9 8160 2007", "32,50", 20, new Manufacturer(1L, "Ache"));

        medicine = repository.save(medicine);

        Assertions.assertNotNull(medicine.getId());
        Assertions.assertEquals(6L, medicine.getId());
    }

    @Test
    @DisplayName("Should return a medicinedto when the id is valid")
    public void findByIdShouldReturnMedicineWhenIdExists() {

        Medicine result = repository.findById(existingId).get();

        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Should not return a medicinedto when the id is valid")
    public void deleteShouldDeleteObjectWhenIdExists() {

        repository.deleteById(existingId);

        Optional<Medicine> result = repository.findById(existingId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    @DisplayName("Should throw an exception when the id is invalid")
    public void deleteShouldThrowEmptyResultDataAccessExceptionWhenIdDoesNotExist() {

        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
            repository.deleteById(nonExistingId);
        });

        Optional<Medicine> result = repository.findById(nonExistingId);
        Assertions.assertFalse(result.isPresent());
    }
}
