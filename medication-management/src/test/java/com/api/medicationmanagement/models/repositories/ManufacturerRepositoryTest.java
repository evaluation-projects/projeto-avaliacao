package com.api.medicationmanagement.models.repositories;

import com.api.medicationmanagement.models.entities.AdverseReactions;
import com.api.medicationmanagement.models.entities.Manufacturer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Optional;

@DataJpaTest
public class ManufacturerRepositoryTest {

    @Autowired
    private ManufacturerRepository repository;

    private long existingId;
    private long nonExistingId;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 1000L;
    }

    @Test
    @DisplayName("Should return a manufacturer when create a new manufacturerdto")
    public void saveShouldPersistWithAutoincrementWhenIdIsNull() {

        Manufacturer manufacturer = new Manufacturer(null,  "Dimed");

        manufacturer = repository.save(manufacturer);

        Assertions.assertNotNull(manufacturer.getId());
        Assertions.assertEquals(7L, manufacturer.getId());
    }

    @Test
    @DisplayName("Should return a manufacturer when the id is valid")
    public void findByIdShouldReturnAdverseReactionsWhenIdExists() {

        Optional<Manufacturer> result = repository.findById(existingId);

        Assertions.assertTrue(result.isPresent());
    }

    @Test
    @DisplayName("Should not return a manufacturer when the id is valid")
    public void deleteShouldDeleteObjectWhenIdExists() {

        repository.deleteById(existingId);

        Optional<Manufacturer> result = repository.findById(existingId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    @DisplayName("Should throw an exception when the id is invalid")
    public void deleteShouldThrowEmptyResultDataAccessExceptionWhenIdDoesNotExist() {

        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
            repository.deleteById(nonExistingId);
        });

        Optional<Manufacturer> result = repository.findById(nonExistingId);
        Assertions.assertFalse(result.isPresent());
    }

}
