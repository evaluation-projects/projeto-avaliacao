package com.api.medicationmanagement.models.repositories;

import com.api.medicationmanagement.models.entities.AdverseReactions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Optional;

@DataJpaTest
public class AdverseReactionsRepositoryTest {

    @Autowired
    private AdverseReactionsRepository repository;

    private long existingId;
    private long nonExistingId;

    @BeforeEach
    void setUp() throws Exception {
        existingId = 1L;
        nonExistingId = 1000L;
    }

    @Test
    @DisplayName("Should return a adversereactionsdto when create a new adversereactionsdto")
    public void saveShouldPersistWithAutoincrementWhenIdIsNull() {

        AdverseReactions adverseReactions = new AdverseReactions(null,  "Dor de cabeça");

        adverseReactions = repository.save(adverseReactions);

        Assertions.assertNotNull(adverseReactions.getId());
        Assertions.assertEquals(8L, adverseReactions.getId());
    }

    @Test
    @DisplayName("Should return a adversereactionsdto when the id is valid")
    public void findByIdShouldReturnAdverseReactionsWhenIdExists() {

        Optional<AdverseReactions> result = repository.findById(existingId);

        Assertions.assertTrue(result.isPresent());
    }


    @Test
    @DisplayName("Should not return a adversereactionsdto when the id is valid")
    public void deleteShouldDeleteObjectWhenIdExists() {

        repository.deleteById(existingId);

        Optional<AdverseReactions> result = repository.findById(existingId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    @DisplayName("Should throw an exception when the id is invalid")
    public void deleteShouldThrowEmptyResultDataAccessExceptionWhenIdDoesNotExist() {

        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
            repository.deleteById(nonExistingId);
        });

        Optional<AdverseReactions> result = repository.findById(nonExistingId);
        Assertions.assertFalse(result.isPresent());
    }


}
