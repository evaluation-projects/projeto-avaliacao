package com.api.medicationmanagement.services;

import com.api.medicationmanagement.dtos.AdverseReactionsDto;
import com.api.medicationmanagement.models.entities.AdverseReactions;
import com.api.medicationmanagement.models.repositories.AdverseReactionsRepository;
import com.api.medicationmanagement.services.exceptions.ResourseNotFoundException;
import com.api.medicationmanagement.services.impl.AdverseReactionsServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {AdverseReactionsServiceImpl.class})
@ExtendWith(MockitoExtension.class)
class AdverseReactionsServiceTest {

    @MockBean
    private AdverseReactionsRepository adverseReactionsRepository;

    @Autowired
    private AdverseReactionsServiceImpl adverseReactionsService;

    @Mock
    private AdverseReactionsRepository repository;

    @InjectMocks
    private AdverseReactionsServiceImpl service;

    @Test
    @DisplayName("Should throw an exception when the id is invalid")
    void findByIdWhenIdIsInvalidThenThrowException() {
        Long id = null;
        when(repository.getById(id)).thenThrow(new ResourseNotFoundException("Id not found " + id));


        Throwable exception =
                org.assertj.core.api.Assertions.catchThrowable(() -> service.findById(id));

        assertThat(exception)
                .isInstanceOf(ResourseNotFoundException.class)
                .hasMessage("Id not found " + id);
    }

    @Test
    @DisplayName("Should return a adversereactionsdto when the id is valid")
    void findByIdWhenIdIsValidThenReturnAdverseReactionsDto() {
        Long id = 1L;
        AdverseReactions adverseReactions = new AdverseReactions(id, "description");
        when(repository.getById(id)).thenReturn(adverseReactions);

        AdverseReactionsDto dto = service.findById(id);

        assertThat(dto.getId()).isEqualTo(id);
        assertThat(dto.getDescription()).isEqualTo("description");
    }


    @Test
    @DisplayName("Should return a page of adversereactionsdto")
    void findAllPagedShouldReturnPageOfAdverseReactionsDto() {
        PageRequest pageRequest = PageRequest.of(0, 10);
        List<AdverseReactions> list = new ArrayList<>();
        list.add(new AdverseReactions(1L, "description"));
        Page<AdverseReactions> page = new PageImpl<>(list);
        when(repository.findAll(pageRequest)).thenReturn(page);

        Page<AdverseReactionsDto> result = service.findAllPaged(pageRequest);

        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getId()).isEqualTo(1L);
        assertThat(result.getContent().get(0).getDescription()).isEqualTo("description");
    }

    @Test
    @DisplayName("Should return a page of adverse reactions")
    void findAllPagedShouldReturnPageOfAdverseReactions() {
        PageRequest pageRequest = PageRequest.of(0, 10);
        List<AdverseReactions> list = new ArrayList<>();
        list.add(new AdverseReactions(1L, "description"));
        Page<AdverseReactions> page = new PageImpl<>(list);
        when(repository.findAll(pageRequest)).thenReturn(page);

        Page<AdverseReactionsDto> result = service.findAllPaged(pageRequest);

        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getId()).isEqualTo(1L);
        assertThat(result.getContent().get(0).getDescription()).isEqualTo("description");
    }
}